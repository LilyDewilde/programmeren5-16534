window.onload = function() {
    var loadMarvelCharacters = function() {
        var ajax;
        if (window.XMLHttpRequest) {
            ajax = new XMLHttpRequest();
        }
        else {
            // code for older browsers
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var me = this;
        // callback functie
        ajax.onreadystatechange = function() {
            // server sent response and
            // OK 200 The request was fulfilled.
            document.write(ajax.readyState + ajax.statusText)
            if (ajax.readyState == 4 && ajax.status == 200) {
                // we maken een tabel element
                showMarvelCharacters(this.responseText);
            }
        };
        ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=e9ce7b0ef20e1ab84733dee3d307dfce", true);
        ajax.send();
    }
    loadMarvelCharacters()
}

function showMarvelCharacters(response) {
    // convert JSON to array
    // we zijn alleen geïnteresseerd in de personnages
    var characters = JSON.parse(response).data.results;
    var table = document.createElement('table');
    characters.map(function(character) {
        var row = document.createElement('tr');
        var name = document.createElement('td');
        var thumbnail = document.createElement('td');
        var imageContent = document.createElement('img');
        imageContent.src = character.thumbnail.path + '.' + character.thumbnail.extension;
        imageContent.style = "height: 150px; width: 150px;"
        var textContent = document.createTextNode(character.name);
        name.appendChild(textContent);
        thumbnail.appendChild(imageContent);
        row.appendChild(name);
        row.appendChild(thumbnail);
        table.appendChild(row);
    });
    document.body.appendChild(table);
}
