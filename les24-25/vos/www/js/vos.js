/**
 * Created by jefin on 20/05/2017.
 */
var vos = {
    'model': {
        loaded: false,
        identity: {},
        procedureList: {},
        organisationList: {},
        position: {},
        myLocation: {}
    },
    'setModel': function() {
        /**
         * plaats JSON bestanden in vos.model.en vos.model.in local storage
         */
        //window.localStorage.clear();
        // zolang je aan het debuggen bent
        //localStorage.removeItem('model');
        // ga na of het model al geladen is
        var model = JSON.parse(localStorage.getItem("model"));
        if (model === null) {
            $http('data/identity.json')
                .get()
                .then(function(data) {
                    vos.model.identity = JSON.parse(data);
                    var payload = {};
                    // procedures depend on Role (in uppercase)
                    var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                    return $http(fileName).get(payload);
                })
                .then(function(data) {
                    vos.model.procedureList = JSON.parse(data);
                    var payload = { 'id': 1 };
                    return $http('data/position.json').get(payload);
                })
                .then(function(data) {
                    vos.model.position = JSON.parse(data);
                    var payload = {};
                    return $http('data/organisationList.json').get(payload);
                })
                .then(function(data) {
                    vos.model.organisationList = JSON.parse(data);
                    vos.model.loaded = true;
                    localStorage.setItem('model', JSON.stringify(vos.model));
                    controller['home']['index'](); // de controller maken we later
                })
                .catch(function(data) {
                    vos.model.loaded = false;
                    localStorage.setItem('model', JSON.stringify(vos.model));
                });
        }
        else {
            vos.model = model;
            controller['home']['index']();
        }
    },
    /**
     * Geolocatie van de telefoon ophalen
     *
     */
    getPosition: function() {
        var options = {
            maximumAge: 3600000,
            timeout: 6000,
            enableHighAccuracy: false
        }
        var onSuccess = function(pos) {
            vos.model.position.latitude = pos.coords.latitude.toFixed(4);
            vos.model.position.longitude = pos.coords.longitude.toFixed(4);
            //vos.setMyLocation();
            //render.identity('#identity');
            //view['home']['index']();
        };
        var onError = function(error) {
            // stel in op hoofdzetel
            vos.model.position.latitude = 51.1771;
            vos.model.position.longitude = 4.3533;
            //vos.setMyLocation();
            //render.identity('#identity');
            //view['home']['index']();
        };
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    },
    /**
     * De dichtsbijzijnde organisatie ophalen.
     * https://stackoverflow.com/questions/21279559/geolocation-closest-locationlat-long-from-my-position
     */
    setMyLocation: function() {
        vos.model.organisationList.forEach(function(item) {
            item.distanceFromMyLocation = getDistanceFromLatLonInKm(
                vos.model.position.latitude, vos.model.position.longitude,
                item.latitude, item.longitude);
        });
        vos.model.organisationList.sort(function(a, b) {
            return a.distanceFromMyLocation - b.distanceFromMyLocation;
        });

        vos.model.organisationList.forEach(
            function(item) {
                document.getElementById("feedback").innerHTML = '';
                document.getElementById("feedback").innerHTML += item.distanceFromMyLocation + ' ' +
                    item.longitude + ' ' + item.latitude + ' ' + item.name + '<br>';
            });
        vos.model.myLocation = vos.model.organisationList[0];
    },
    /**
     * nagaan of de gebruikersnaam en het paswoord in de personList zitten
     * dit is geen veilige manier om aan te melden en moet je niet in productie app gebruiken
     */
    login: function() {
        var userName = document.getElementById('userName').value;
        var password = document.getElementById('password').value;

        if (userName) {
            $http('data/personList.json')
                .get()
                .then(function(responseText) {
                    var person = JSON.parse(responseText);
                    var userIdentity = person.list.find(function(item) {
                        return item.userName === userName && item.password === password
                    });
                    if (userIdentity) {
                        userIdentity.loggedIn = true;
                        // identity = JSON.parse(localStorage.getItem('identity'));
                        vos.model.identity = userIdentity;
                        var payload = {};
                        // procedures depend on Role (in uppercase)
                        var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                        $http(fileName).get(payload)
                            .then(function(data) {
                                vos.model.procedureList = JSON.parse(data);
                                localStorage.setItem('model', JSON.stringify(vos.model));
                                controller['home']['index']();
                            });
                    }
                    else {
                        alert('ongeldige gebruikersnaam of paswoord');
                    }
                })
                .catch(function(message) {
                    alert(message);
                })
        }
        else {
            alert('Je moet een gebruikersnaam opgeven!');
        }
    },
    //login met google
    googleLogin: function(id) {
        $http('data/personList.json')
            .get()
            .then(function(responseText) {
                var person = JSON.parse(responseText);
                var userIdentity = person.list.find(function(item) {
                    return item.code === id
                });
                if (userIdentity) {
                    userIdentity.loggedIn = true;
                    // identity = JSON.parse(localStorage.getItem('identity'));
                    vos.model.identity = userIdentity;
                    var payload = {};
                    // procedures depend on Role (in uppercase)
                    var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                    $http(fileName).get(payload)
                        .then(function(data) {
                            vos.model.procedureList = JSON.parse(data);
                            localStorage.setItem('model', JSON.stringify(vos.model));
                            controller['home']['index']();
                        });
                }
                else {
                    alert('ongeldige google ID');
                }
            })
            .catch(function(message) {
                alert(message);
            })
    },
    /**
     * de property loggedIn van identity op false zetten
     * en role op GAST
     */
    logout: function() {
        $http('data/identity.json')
            .get()
            .then(function(responseText) {
                vos.model.identity = JSON.parse(responseText);
                var payload = {};
                // procedures depend on Role (in uppercase)
                var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                return $http(fileName).get(payload);
            })
            .then(function(responseText) {
                vos.model.procedureList = JSON.parse(responseText);
                vos.model.loaded = true;
                localStorage.setItem('model', JSON.stringify(vos.model));
                controller['home']['index']();
            })
            .catch(function(message) {
                alert(message);
            });
    },
    /**
     * Ga naar de floor met de opgegeven view id en toon de tekst
     * in title in het h1 element.
     *
     * @param {string} view text id van de floor die getoond moet worden.
     * @param {string} title tekst die in het h1 element geplaatst moet worden.
     */
    navigateTo: function(view, title) {
        window.location.href = '#' + view;
        var h1 = document.querySelector('#' + view + ' h1');
        if (title && h1) {
            h1.innerHTML = title;
        }
    },
    /**
     * Voorbereiding versturen sms.
     *
     * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
     * @param {string} messageText boodschap die verstuurd zal worden.
     */
    'smsPrepare': function(number, messageText, procedureCode, stepName) {
        // number = '0486788723';
        var message = messageText + '\n' +
            vos.model.identity.firstName + ' ' + vos.model.identity.lastName + '\n' +
            vos.model.myLocation.name + '\n' +
            vos.model.myLocation.street + '\n' +
            vos.model.myLocation.postalCode + ' ' + vos.model.myLocation.city + '\n' +
            number;
        //smsSend(number, message);
        //debugnumber = 0491324991
        smsSend('0491324991', message);
        let requestBody = '"\'SMS\',\'' + vos.model.identity.mobile + '\',\'' + vos.model.identity.email + '\',\'' + procedureCode + '\',\'' + messageText + '\',\'' + vos.model.identity.role + '\',\'' + number + '\',\'' + stepName + '\',\'' + vos.model.identity.userName + '\'"';
        $.ajax({
            url: 'http://Programmeren5-16534-lilydewilde-lilydew.c9users.io:8081/api/log',
            type: 'PUT',
            data: requestBody,
            crossDomain: true,
            contentType: 'application/json',
        });
    },
    'callPrepare': function(number, procedureCode, procedureTitle, stepTitle) {
        //phoneCall(number);
        //static number for debugging 0491324991
        phoneCall('0491324991');
        let requestBody = '"\'TEL\',\'' + vos.model.identity.mobile + '\',\'' + vos.model.identity.email + '\',\'' + procedureCode + '\',\'' + procedureTitle + '\',\'' + vos.model.identity.role + '\',\'' + number + '\',\'' + stepTitle + '\',\'' + vos.model.identity.userName + '\'"';
        $.ajax({
            url: 'http://Programmeren5-16534-lilydewilde-lilydew.c9users.io:8081/api/log',
            type: 'PUT',
            data: requestBody,
            contentType: 'application/json',
            dataType: 'application/json'
        });
    },
    settings: function() {
        checkConnection();
    }
};

var view = {
    'home': {
        'index': function() {
            window.location.assign("index.html#home-index");
        },
        'loggingIn': function() {
            window.location.href = "#home-loggingIn";
        }
    },
    'psycho-social-risk': {
        'index': function() {
            window.location.href = "#psycho-social-index";
        }
    },
    'fire': {
        'index': function() {
            window.location.href = "#fire-index";
        }
    },
    'terror': {
        'index': function() {
            window.location.href = "#terror-index";
        }
    },
    'accident': {
        'index': function() {
            window.location.href = "#accident-index";
        }
    },
    'chlorine-leak': {
        'index': function() {
            window.location.href = "#chlorine-leak-index";
        }
    },
    'procedure': function(title) {
        vos.navigateTo('view-procedure', title);
    }
};

var render = {
    'identity': function(querySelector) {
        var elem = document.querySelector(querySelector);
        elem.innerHTML = '';
        elem.appendChild(makeTextElement(vos.model.identity.firstName + ' ' + vos.model.identity.lastName, 'h2'))
        elem.appendChild(makeTextElement(vos.model.identity.function, 'h3'));
        elem.appendChild(makeTextElement(vos.model.identity.mobile, 'h4'));
        elem.appendChild(makeTextElement(vos.model.myLocation.name));
        elem.appendChild(makeTextElement(vos.model.myLocation.street));
        elem.appendChild(makeTextElement(vos.model.myLocation.phone));
        if (vos.model.identity.loggedIn) {
            elem.appendChild(makeTextElement('aangemeld als ' + vos.model.identity.role));
            var buttonElement = makeButton('Afmelden');
            buttonElement.setAttribute('name', 'uc');
            buttonElement.setAttribute('value', 'home/logout');
            elem.appendChild(buttonElement);

        }
        else {
            elem.appendChild(makeTextElement('aangemeld als gast'));
            var buttonElement = makeButton('Aanmelden');
            buttonElement.setAttribute('name', 'uc');
            buttonElement.setAttribute('value', 'home/loggingIn');
            elem.appendChild(buttonElement);
        }
        return elem;
    },
    'procedure': {
        'make': function(procedureCode) {
            var procedure = vos.model.procedureList.procedure.find(function(item) {
                return item.code === procedureCode;
            });
            elem = render.identity('#view-procedure .show-room');
            var step = document.createElement('DIV');
            step.setAttribute('class', 'step');
            step.appendChild(makeHtmlTextElement(procedure.heading, 'h2'));
            var listElement = document.createElement('OL');
            listElement.setAttribute('class', 'index');
            procedure.step.forEach(function(item, index) {
                var step = makeHtmlTextElement(item.title, 'li');
                let stepName = item.title;
                if ("action" in item) {
                    var commandPanelElem = makeCommandPanel();
                    item.action.forEach(function(item) {
                        commandPanelElem.appendChild(render.procedure[item.code](item, procedure.title, procedureCode, stepName));
                    });
                    step.appendChild(commandPanelElem);
                }
                if ("list" in item) {
                    step.appendChild(render.procedure['LIST'](item.list));
                }
                listElement.appendChild(step);
            });
            step.appendChild(listElement);
            elem.appendChild(step);
        },
        'TEL': function(item, message, procedureCode, stepName) {
            // Het telefoonnummer van directie, secretariaat, ... is afhankelijk van de plaats
            var phoneNumber = getPhoneNumber(item.phoneNumber);
            if (vos.model.identity.loggedIn) {
                var buttonElement = makeTileButton('Tel', 'icon-phone');
                buttonElement.addEventListener('click', function() {
                    vos.callPrepare(phoneNumber, procedureCode, message, stepName);
                    //phoneCall(phoneNumber);
                });
                return buttonElement;
            }
            else {
                return makeTextElement(item.code + ' ' + phoneNumber, 'P');
            }
        },
        'SMS': function(item, message, procedureCode, stepName) {
            // Het telefoonnummer van directie, secretariaat, ... is afhankelijk van de plaats
            var phoneNumber = getPhoneNumber(item.phoneNumber);
            if (vos.model.identity.loggedIn) {
                var buttonElement = makeTileButton('Tel', 'icon-send');
                buttonElement.addEventListener('click', function() {
                    vos.smsPrepare(phoneNumber, message, procedureCode, stepName);
                });
                return buttonElement;
            }
            else {
                return makeTextElement(item.code + ' ' + phoneNumber, 'P');
            }
        },
        'LIST': function(list) {
            var listElement = document.createElement('OL');
            listElement.setAttribute('class', 'index');
            list.forEach(function(item) {
                listElement.appendChild(makeHtmlTextElement(item.title, 'li'))
            });
            return listElement;
        }
    }
};

var controller = {
    'home': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#home .identity');
            view['home']['index']();
        },
        'gas-leak': function() {
            render.procedure.make('GL');
            view['procedure']('gaslek');
        },
        'amok': function() {
            render.procedure.make('AMOK');
            view['procedure']('AMOK - Geweld');
        },
        'loggingIn': view['home']['loggingIn'],
        'login': function() {
            vos.login();
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#home .identity');
            view['home']['index']();
        },
        'googlelogin': function() {
            signin();
            /*vos.getPosition();
            vos.setMyLocation();
            render.identity('#home .identity');
            view['home']['index']();*/
        },
        'logout': function() {
            vos.logout();
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#home .identity');
            view['home']['index']();
        },
        'settings': vos.settings
    },
    'call': {
        'hot-line': function() {
            //phoneCall('+32486788723');
            //window.open('tel:+32486788723');
            vos.callPrepare('0491324991', 'CALL EMERGENCY', 'Hulpdiensten bellen', 'oproep starten');
        }
    },
    'psycho-social-risk': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#psycho-social-risk .identity');
            view['psycho-social-risk']['index']();
        }
    },
    'terror': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#terror .identity');
            view['terror']['index']();
        },
        'bomb-alarm': function() {
            render.procedure.make('BA');
            view['procedure']('bomalarm');
        },
        'suspicious-object': function() {
            render.procedure.make('VV');
            view['procedure']('verdacht voorwerp');
        },
        'terrorist-attack': function() {
            render.procedure.make('TA');
            view['procedure']('terroristische aanslag');
        },
        'amok': function() {
            render.procedure.make('AMOK');
            view['procedure']('AMOK & blind geweld');
        }
    },
    'accident': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#accident .identity');
            view['accident']['index']();
        },
        'extra-muros': function() {
            render.procedure.make('EM');
            view['procedure']('extra-muros');
        },
        'serious-work-accident': function() {
            render.procedure.make('SWA');
            view['procedure']('ernstig arbeidsongeval');
        },
        'work-accident': function() {
            render.procedure.make('WA');
            view['procedure']('arbeidsongeval');
        },
        'to-from-school': function() {
            render.procedure.make('TFS');
            view['procedure']('van en naar school');
        }
    },
    'fire': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#fire .identity');
            view['fire']['index']('brand');
        },
        'detection': function() {
            render.procedure.make('BM');
            view['procedure']('brandmelding');
        },
        'evacuation': function() {
            render.procedure.make('BREV');
            view['procedure']('brandevacuatie');
        }
    },
    'chlorine-leak': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#chlorine-leak .identity');
            view['chlorine-leak']['index']('chloorlek');
        },
        'chlorine-leak-water': function() {
            render.procedure.make('CLW');
            view['procedure']('chloorlek water');
        },
        'chlorine-leak-air': function() {
            render.procedure.make('CLA');
            view['procedure']('chloorlek lucht');
        }
    },
    'page': {
        'previous': function() {
            window.history.back();
        }
    }
};

/**
 * Dispath methode die de use case uitvoert die overeenkomt
 * met een de gevraagde interactie van de gebruiker.
 *
 * @param {object} e verwijzing naar het dom element dat het event heeft afgevuurd.
 */
var dispatcher = function(e) {
    var target = e.target;
    var steps = 0;
    while (target.getAttribute('name') !== 'uc' && steps < 5 && target.tagName !== 'BODY') {
        target = target.parentNode;
        steps++;
    }
    if (target.getAttribute('name') === 'uc') {
        var uc = target.getAttribute('value');
        var path = uc.split('/');
        var entity = path[0] === undefined ? 'none' : path[0];
        var action = path[1] === undefined ? 'none' : path[1];
        var view = entity + '-' + action;
        // alert (entity + '/' + action);
        if (controller[entity][action]) {
            controller[entity][action]();
        }
        else {
            alert('ongeldige url ' + uc);
        }
    }
};

document.body.addEventListener('click', dispatcher, false);
vos.setModel();
