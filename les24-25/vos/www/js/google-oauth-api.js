var clientId = '761463076686-cmglfmennut07iefu8bhhp6macaranoj.apps.googleusercontent.com';
var apiKey = 'AIzaSyDRYcMFsN7qPi5tvreV7sunpyNrBhSlQKA';
var scopes = 'profile';

var signinButton = document.getElementById('signinButton');

function handleClientLoad() {
    // Load the API client and auth library
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    // gapi.client.setApiKey(apiKey);
    gapi.auth2.init({
        'apiKey': apiKey,
        client_id: clientId,
        scope: scopes
    }).catch(function(e) {
        window.alert(e.details);
    });
}

function signin() {
    gapi.auth2.getAuthInstance().signIn().then((response) => {
        let profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
        vos.googleLogin(profile.getId());
        var auth2 = gapi.auth2.getAuthInstance();
        return auth2.signOut().then(function() {
            auth2.disconnect();
        });
    });
}

/*function signin() {
    gapi.auth2.getAuthInstance().signIn()
    let profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    vos.googleLogin(profile.getId());
    var auth2 = gapi.auth2.getAuthInstance();
    return auth2.signOut().then(function() {
        auth2.disconnect();
    });
}*/


function signout() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        auth2.disconnect();
    });
}

//108854781815403825770
//118358154924786981542 (mob)
