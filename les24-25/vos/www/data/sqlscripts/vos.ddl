CREATE TABLE Log (
    UserName CHAR(50) NOT NULL,
    Email CHAR(255) NOT NULL,
    Role CHAR(50) NOT NULL,
    ProcedureCode CHAR(25) NOT NULL,
    ProcedureTitle CHAR(255) NOT NULL,
    StepTitle CHAR(255) NOT NULL,
    ActionCode CHAR(10) NOT NULL,
    CallNumber CHAR(25) NOT NULL,
    SendNumber CHAR(25) NOT NULL,
    ID INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(ID))