﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VosAPI.Models
{
    public class LogItem
    {
        public int ID { get; set; }
        public string ActionCode { get; set; }
        public string CallNumber { get; set; }
        public string Email { get; set; }
        public string ProcedureCode { get; set; }
        public string ProcedureTitle { get; set; }
        public string Role { get; set; }
        public string SendNumber { get; set; }
        public string StepTitle { get; set; }
        public string UserName { get; set; }
    }
}
