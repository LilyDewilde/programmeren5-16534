﻿using Microsoft.EntityFrameworkCore;

namespace VosAPI.Models
{
    public class LogContext : DbContext
    {
        public LogContext(DbContextOptions<LogContext> options)
            : base(options)
        {
        }

        public DbSet<LogItem> LogItems { get; set; }
    }
}