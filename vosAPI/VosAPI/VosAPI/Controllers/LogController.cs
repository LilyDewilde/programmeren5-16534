﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using VosAPI.Models;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Data;

namespace VosAPI.Controllers
{
    [Route("api/log")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly LogContext _context;
        string ConnectionString = "Server=localhost;user id=root;password=;port=3306;database=Vos;";

        public TodoController(LogContext context)
        {
            _context = context;
        }
        // GET: api/log
        [HttpGet]
        public string GetLogItems()
        {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            string logList = "Gebruikersnaam\tE-mail\tRol\tProcedure code\tProcedure titel\tStep titel\tActie code\tOproepnummer\tOpgeroepen Nummer";
            using (connection){
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "CALL LeesAlles();";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        logList = logList + Environment.NewLine + reader["Gebruikersnaam"]+ "\t" + reader["E-mail"]+ "\t" + reader["Rol"]+ "\t" + reader["Procedure code"]+ "\t" + reader["Procedure titel"]+ "\t" + reader["Step titel"]+ "\t" + reader["Actie code"]+ "\t" + reader["Oproepnummer"]+ "\t" + reader["Opgeroepen Nummer"];
                        Console.WriteLine("adding more");
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();    
            }
            return logList;
        }

        // GET: api/log/id
        [HttpGet("{id}")]
        public string GetLogItem(int id)
        {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            string returnString = "";
            using (connection){
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "CALL LeesEen(" + id + ");";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    if (reader.Read())
                    {
                        returnString = reader["Gebruikersnaam"]+ "\t" + reader["E-mail"]+ "\t" + reader["Rol"]+ "\t" + reader["Procedure code"]+ "\t" + reader["Procedure titel"]+ "\t" + reader["Step titel"]+ "\t" + reader["Actie code"]+ "\t" + reader["Oproepnummer"]+ "\t" + reader["Opgeroepen Nummer"];
                        Console.WriteLine("adding more");
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();    
            }
            return returnString;
        }
        
        // DELETE: api/log/id
        [HttpDelete("{id}")]
        public string Delete(int id) {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            using (connection){
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "CALL DeleteEen(" + id + ");";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return "success probably";
            }
            return "failure i'm sure";
        }
        

        
        // PUT: api/log
        [HttpPut]
        public string AddLogItem([FromBody] string item)
        {
            //char[] delim = {','};
            //item.Split(delim, 9, StringSplitOptions.None);
            //INSERT INTO Log VALUES(p_ActionCode, p_CallNumber, p_Email, p_ProcedureCode, p_ProcedureTitle, p_Role, p_SendNumber, p_StepTitle, p_UserName)
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            using (connection){
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "CALL InsertRij(" + item + ");";
                // http put req example string: "'cc','cc','cc','cc','cc','cc','cc','cc','cc'"
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return "success probably";
            }
        }
        
        // PUT: api/log/id or api/log/username
        [HttpPut("{input}")]
        public string EditLogItem(string input, [FromBody] string item) {
            if(input.All(Char.IsDigit)) {
                MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
                connection.ConnectionString = ConnectionString;
                Console.WriteLine("Connectie gemaakt.");
                using (connection){
                    MySqlCommand command = new MySqlCommand();
                    //UpdateEenID(i_ID INT(20), p_ActionCode CHAR(10), p_CallNumber CHAR(25), p_Email CHAR(255), p_ProcedureCode CHAR(25), p_ProcedureTitle CHAR(255), p_Role CHAR(50), p_Sendnumber CHAR(25), p_Steptitle CHAR(255), p_Username CHAR(50))
                    string sqlStatement = "CALL UpdateEenID('" + input + "'," + item + ");";
                    command.Connection = connection;
                    command.CommandText = sqlStatement;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                    return "success probably";
                }
            } else {
                MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
                connection.ConnectionString = ConnectionString;
                Console.WriteLine("Connectie gemaakt.");
                using (connection){
                    MySqlCommand command = new MySqlCommand();
                    //UpdateEenID(i_ID INT(20), p_ActionCode CHAR(10), p_CallNumber CHAR(25), p_Email CHAR(255), p_ProcedureCode CHAR(25), p_ProcedureTitle CHAR(255), p_Role CHAR(50), p_Sendnumber CHAR(25), p_Steptitle CHAR(255), p_Username CHAR(50))
                    string sqlStatement = "CALL UpdateEenUserName('" + input + "'," + item + ");";
                    command.Connection = connection;
                    command.CommandText = sqlStatement;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                    return "success probably";
                }
            }
        }
    }    
}