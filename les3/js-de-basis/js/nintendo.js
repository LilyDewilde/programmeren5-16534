var timer;
var timeline;
var timelineLeft = 0;
var currentTimeline = 1;


var startAnim = function() {
    timer = window.setInterval(animate, 10);
    timeline = document.getElementById('timeline');
}

var animate = function() {
    timelineLeft += 2;
    timeline.style.left = timelineLeft + 'px';
    switch (currentTimeline) {
        case 1:
            if (timelineLeft >= window.innerWidth / 6) {
                currentTimeline = 2;
                timeline.src = "./files/375px-Game-Boy-Original.jpg";
            }
            break;
        case 2:
            if (timelineLeft >= 2 * window.innerWidth / 6) {
                currentTimeline = 3;
                timeline.src = "./files/800px-N64-Console-Set.jpg";
            }
            break;
        case 3:
            if (timelineLeft >= 3 * window.innerWidth / 6) {
                currentTimeline = 4;
                timeline.src = "./files/800px-GameCube-Set.jpg";
            }
            break;
        case 4:
            if (timelineLeft >= 4 * window.innerWidth / 6) {
                currentTimeline = 5;
                timeline.src = "./files/711px-Nintendo-DS-Lite-Black-Open.jpg";
            }
            break;
        case 5:
            if (timelineLeft >= 5 * window.innerWidth / 6) {
                currentTimeline = 6;
                timeline.src = "./files/800px-Nintendo-Switch-Console-Docked-wJoyConRB.jpg";
            }
            break;
        case 6:
            if (timelineLeft >= window.innerWidth) {
                window.clearInterval(timer);
                timeline.style.display = 'none';
            }
            break;
    }
}